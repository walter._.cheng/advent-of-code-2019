#include <fstream>
#include <cstdio>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

void inputvectorlist(ifstream& inFile, vector<string>& inputvector, int&count);
void readvector(vector<string> inputvector);
void mathalgorithm(vector<string>& inputvector, vector<int>& outputvector);

void inputvectorlist(ifstream& inFile, vector<string>& inputvector, int& count){
  string line;
  if (inFile.is_open()){
    while (getline(inFile, line)){
      ++count;
      inputvector.resize(count);
      inputvector[count - 1] = line;
    }
    inFile.close();
  }
  else cout << "Unable to open file";
}

void readvector(vector<string> inputvector){
  for(int i = 0; i < inputvector.size(); ++i){
    cout << inputvector[i] << "\n";
  }
}

void mathalgorithm(vector<string>& inputvector, vector<int>& outputvector){
string temp;
int x = 0;
int strawberrywaffles;
  for(int i = 0; i < inputvector.size(); ++i){
    x = 0;
    temp = inputvector[i];
    strawberrywaffles = stoi(temp);
    while(strawberrywaffles > 0){
      strawberrywaffles = (strawberrywaffles/3) - 2;
      if(strawberrywaffles > 0){
        x = x + strawberrywaffles;
      }
    }
    outputvector.resize(i+1);
    outputvector[i] = x;
  }
}

int main(){
  int output = 0;
  int count = 0;
  vector<string> inputvector;
  vector<int> outputvector;
  ifstream inFile ("AdventDay1Input");
  inputvectorlist(inFile, inputvector, count);
  mathalgorithm(inputvector, outputvector);
  for(int i = 0; i < outputvector.size(); ++i){
    output = output + outputvector[i];
  }
  cout << output << "\n";
  return 0;
}
