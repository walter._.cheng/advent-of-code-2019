#include <fstream>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

void getinputrange(ifstream& inFile, int& min, int& max);
void checknum(bool& hasdouble, bool& increase, vector<int> digits);
void storedigits(int x, vector<int>& digits, int& count);

void getinputrange(ifstream& inFile, int& min, int& max){
  string line;
  string input;
  if(inFile.is_open()){
    while(getline(inFile, line)){
      input = line;
    }
    inFile.close();
  }
  else cout << "Unable to open file";

  stringstream ss(input);
  int count = 0;
  while(ss.good()){
    string substr;
    getline(ss, substr, '-');
    if(count == 0){
      min = stoi(substr);
    }
    else if(count == 1){
      max = stoi(substr);
    }
    ++count;
  }
}

void storedigits(int x, vector<int>& digits, int& count){
  if(x >= 10){
     storedigits(x / 10, digits, count);
  }
  int digit = x % 10;
  digits.resize(count+1);
  digits[count] = digit;
  ++count;
}

void checknum(bool& hasdouble, bool& increase, vector<int> digits){
  for(int i = 0; i < digits.size()-1; ++i){
    if(digits[i] == digits[i+1]){
      hasdouble = true;
    }
    if(digits[i] > digits[i+1]){
      increase = false;
      break;
    }
    else increase = true;
  }
}

int main(){
  int min = 0;
  int max = 0;
  vector<int> digits;
  int count = 0;
  bool hasdouble;
  bool increase;
  ifstream inFile("AdventDay4Input");

  getinputrange(inFile, min, max);

  int finalcount = 0;

  for(int i = min; i < max; ++i){
    storedigits(i, digits, count);
    checknum(hasdouble, increase, digits);
    if(hasdouble == true && increase == true){
      ++finalcount;
    }
    digits.erase(digits.begin(), digits.end());
    count = 0;
    hasdouble = false;
    increase = false;
  }

  cout << finalcount;

  return 0;
}
