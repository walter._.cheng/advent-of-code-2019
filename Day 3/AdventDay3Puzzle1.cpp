#include <fstream>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <bits/stdc++.h>

using namespace std;

void inputvectorlist(ifstream& inFile, vector<string>& inputvector);
void parseinput(string input, vector<string>& inputvector);

void inputvectorlist(ifstream& inFile, vector<string>& inputvector){
  int count = 0;
  string line;
  if (inFile.is_open()){
    while (getline(inFile, line)){
      ++count;
      inputvector.resize(count);
      inputvector[count - 1] = line;
    }
    inFile.close();
  }
  else cout << "Unable to open file";
}

void parseinput(string input, vector<string>& inputvector){
  stringstream ss(input);
  int count = 0;
  while(ss.good()){
    string substr;
    getline(ss, substr, ',');
    inputvector.resize(count+1);
    inputvector.at(count) = substr;
    ++count;
  }
}

int main(){
  //definitions
  ifstream inFile ("AdventDay3Input");
  vector<string> inputvector;
  vector<string> wire1vector;
  vector<string> wire2vector;
  vector<int> wire1xtracker;
  vector<int> wire1ytracker;
  vector<int> wire2xtracker;
  vector<int> wire2ytracker;
  vector<int> finalxvector;
  vector<int> finalyvector;
  vector<int> finalvector;
  string temp;
  int pointerx = 0;
  int pointery = 0;
  int count = 0;

  inputvectorlist(inFile, inputvector);

  temp = inputvector[0];
  parseinput(temp, wire1vector);
  temp = inputvector[1];
  parseinput(temp, wire2vector);

//putting all the values into the tracker
  for(int i = 0; i < wire1vector.size(); ++i){
    if(wire1vector[i].front() == 'U'){
      temp = wire1vector[i];
      temp.erase(0,1);
      for(int x = 0; x < stoi(temp); ++x){
        wire1xtracker.resize(count+1);
        wire1ytracker.resize(count+1);
        ++pointery;
        wire1xtracker[count] = pointerx;
        wire1ytracker[count] = pointery;
        ++count;
      }
    }
    else if(wire1vector[i].front() == 'D'){
      temp = wire1vector[i];
      temp.erase(0,1);
      for(int x = 0; x < stoi(temp); ++x){
        wire1xtracker.resize(count+1);
        wire1ytracker.resize(count+1);
        --pointery;
        wire1xtracker[count] = pointerx;
        wire1ytracker[count] = pointery;
        ++count;
      }
    }
    else if(wire1vector[i].front() == 'R'){
      temp = wire1vector[i];
      temp.erase(0,1);
      for(int x = 1; x <= stoi(temp); ++x){
        wire1xtracker.resize(count+1);
        wire1ytracker.resize(count+1);
        ++pointerx;
        wire1xtracker[count] = pointerx;
        wire1ytracker[count] = pointery;
        ++count;
      }
    }
    else if(wire1vector[i].front() == 'L'){
      temp = wire1vector[i];
      temp.erase(0,1);
      for(int x = 0; x < stoi(temp); ++x){
        wire1xtracker.resize(count+1);
        wire1ytracker.resize(count+1);
        --pointerx;
        wire1xtracker[count] = pointerx;
        wire1ytracker[count] = pointery;
        ++count;
      }
    }
  }

  pointerx = 0;
  pointery = 0;
  count = 0;

  for(int i = 0; i < wire2vector.size(); ++i){
    if(wire2vector[i].front() == 'U'){
      temp = wire2vector[i];
      temp.erase(0,1);
      for(int x = 0; x < stoi(temp); ++x){
        wire2xtracker.resize(count+1);
        wire2ytracker.resize(count+1);
        ++pointery;
        wire2xtracker[count] = pointerx;
        wire2ytracker[count] = pointery;
        ++count;
      }
    }
    else if(wire2vector[i].front() == 'D'){
      temp = wire2vector[i];
      temp.erase(0,1);
      for(int x = 0; x < stoi(temp); ++x){
        wire2xtracker.resize(count+1);
        wire2ytracker.resize(count+1);
        --pointery;
        wire2xtracker[count] = pointerx;
        wire2ytracker[count] = pointery;
        ++count;
      }
    }
    else if(wire2vector[i].front() == 'R'){
      temp = wire2vector[i];
      temp.erase(0,1);
      for(int x = 0; x < stoi(temp); ++x){
        wire2xtracker.resize(count+1);
        wire2ytracker.resize(count+1);
        ++pointerx;
        wire2xtracker[count] = pointerx;
        wire2ytracker[count] = pointery;
        ++count;
      }
    }
    else if(wire2vector[i].front() == 'L'){
      temp = wire2vector[i];
      temp.erase(0,1);
      for(int x = 0; x < stoi(temp); ++x){
        wire2xtracker.resize(count+1);
        wire2ytracker.resize(count+1);
        --pointerx;
        wire2xtracker[count] = pointerx;
        wire2ytracker[count] = pointery;
        ++count;
      }
    }
  }

  count = 0;

  for(int i = 0; i < wire2xtracker.size(); ++i){
    for(int x = 0; x < wire1xtracker.size(); ++x){
      if(wire2xtracker[i] == wire1xtracker[x]){
        if(wire2ytracker[i] == wire1ytracker[x]){
          finalxvector.resize(count+1);
          finalyvector.resize(count+1);
          finalxvector[count] = wire2xtracker[i];
          finalyvector[count] = wire2ytracker[i];
          ++count;
        }
      }
    }
  }

  for(int i = 0; i < finalxvector.size(); ++i){
    finalvector.resize(i+1);
    finalvector[i] = abs(finalxvector[i]) + abs(finalyvector[i]);
  }

  cout << *min_element(finalvector.begin(), finalvector.end());

  return 0;
}
