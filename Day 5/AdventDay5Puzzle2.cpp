#include <fstream>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

void inputvectorlist(ifstream& inFile, string& input);
void parseinput(string input, vector<int>& inputvector);
void dostuff(vector<int>& inputvector, int opinput);
void storedigits(int x, vector<int>& digits, int& count);

void inputvectorlist(ifstream& inFile, string& input){
  string line;
  if (inFile.is_open()){
    while (getline(inFile, line)){
      input = line;
    }
    inFile.close();
  }
  else cout << "Unable to open file";
}

void parseinput(string input, vector<int>& inputvector){
  stringstream ss(input);
  int count = 0;
  while(ss.good()){
    string substr;
    getline(ss, substr, ',');
    inputvector.resize(count+1);
    inputvector[count] = stoi(substr);
    ++count;
  }
}

void storedigits(int x, vector<int>& digits, int& count){
  if(x >= 10){
     storedigits(x / 10, digits, count);
  }
  int digit = x % 10;
  digits.resize(count+1);
  digits[count] = digit;
  ++count;
}


void dostuff(vector<int>& inputvector, int opinput){
  int positioncount = 0;
  int temp;
  int count = 0;
  vector<int> digits;
  vector<int> parametermodes;
  while(true){
    storedigits(inputvector[positioncount], digits, count);
    temp = 5 - digits.size();
    for(int i = 0; i < temp; ++i){
      digits.insert(digits.begin(), 0);
    }
    for(int i = 0; i < 3; ++i){
      parametermodes.insert(parametermodes.begin(), digits[i]);
    }

    if(digits[digits.size()-1] == 1){
      if(parametermodes[0] == 1 && parametermodes[1] == 1){
        temp = inputvector[positioncount+1] + inputvector[positioncount+2];
        inputvector[inputvector[positioncount+3]] = temp;
        positioncount += 4;
      }
      else if(parametermodes[0] == 1){
        temp = inputvector[positioncount+1] + inputvector[inputvector[positioncount+2]];
        inputvector[inputvector[positioncount+3]] = temp;
        positioncount += 4;
      }
      else if(parametermodes[1] == 1){
        temp = inputvector[inputvector[positioncount+1]] + inputvector[positioncount+2];
        inputvector[inputvector[positioncount+3]] = temp;
        positioncount += 4;
      }
      else{
        temp = inputvector[inputvector[positioncount+1]] + inputvector[inputvector[positioncount+2]];
        inputvector[inputvector[positioncount+3]] = temp;
        positioncount += 4;
      }
    }
    else if(digits[digits.size()-1] == 2){
      if(parametermodes[0] == 1 && parametermodes[1] == 1){
        temp = inputvector[positioncount+1] * inputvector[positioncount+2];
        inputvector[inputvector[positioncount+3]] = temp;
        positioncount += 4;
      }
      else if(parametermodes[0] == 1){
        temp = inputvector[positioncount+1] * inputvector[inputvector[positioncount+2]];
        inputvector[inputvector[positioncount+3]] = temp;
        positioncount += 4;
      }
      else if(parametermodes[1] == 1){
        temp = inputvector[inputvector[positioncount+1]] * inputvector[positioncount+2];
        inputvector[inputvector[positioncount+3]] = temp;
        positioncount += 4;
      }
      else{
        temp = inputvector[inputvector[positioncount+1]] * inputvector[inputvector[positioncount+2]];
        inputvector[inputvector[positioncount+3]] = temp;
        positioncount += 4;
      }
    }
    else if(digits[digits.size()-1] == 3){
      inputvector[inputvector[positioncount+1]] = opinput;
      positioncount +=2;
    }
    else if(digits[digits.size()-1] == 4){
      if(parametermodes[0] == 1){
        cout << inputvector[positioncount+1] << "\n";
        positioncount +=2;
      }
      else{
        cout << inputvector[inputvector[positioncount+1]] << "\n";
        positioncount +=2;
      }
    }
    else if(digits[digits.size()-1] == 5){
      if(parametermodes[0] == 1 && parametermodes[1] == 1){
        if(inputvector[positioncount+1] != 0){
          positioncount = inputvector[positioncount+2];
        }
        else{
          positioncount += 3;
        }
      }
      else if(parametermodes[0] == 1){
        if(inputvector[positioncount+1] != 0){
          positioncount = inputvector[inputvector[positioncount+2]];
        }
        else{
          positioncount += 3;
        }
      }
      else if(parametermodes[1] == 1){
        if(inputvector[inputvector[positioncount+1]] != 0){
          positioncount = inputvector[positioncount+2];
        }
        else{
          positioncount += 3;
        }
      }
      else{
        if(inputvector[inputvector[positioncount+1]] != 0){
          positioncount = inputvector[inputvector[positioncount+2]];
        }
        else{
          positioncount += 3;
        }
      }
    }
    else if(digits[digits.size()-1] == 6){
      if(parametermodes[0] == 1 && parametermodes[1] == 1){
        if(inputvector[positioncount+1] == 0){
          positioncount = inputvector[positioncount+2];
        }
        else{
          positioncount += 3;
        }
      }
      else if(parametermodes[0] == 1){
        if(inputvector[positioncount+1] == 0){
          positioncount = inputvector[inputvector[positioncount+2]];
        }
        else{
          positioncount += 3;
        }
      }
      else if(parametermodes[1] == 1){
        if(inputvector[inputvector[positioncount+1]] == 0){
          positioncount = inputvector[positioncount+2];
        }
        else{
          positioncount += 3;
        }
      }
      else{
        if(inputvector[inputvector[positioncount+1]] == 0){
          positioncount = inputvector[inputvector[positioncount+2]];
        }
        else{
          positioncount += 3;
        }
      }
    }
    else if(digits[digits.size()-1] == 7){
      if(parametermodes[0] == 1 && parametermodes[1] == 1){
        if(inputvector[positioncount+1] < inputvector[positioncount+2]){
          inputvector[inputvector[positioncount+3]] = 1;
          positioncount += 4;
        }
        else{
          inputvector[inputvector[positioncount+3]] = 0;
          positioncount += 4;
        }
      }
      else if(parametermodes[0] == 1){
        if(inputvector[positioncount+1] < inputvector[inputvector[positioncount+2]]){
          inputvector[inputvector[positioncount+3]] = 1;
          positioncount += 4;
        }
        else{
          inputvector[inputvector[positioncount+3]] = 0;
          positioncount += 4;
        }
      }
      else if(parametermodes[1] == 1){
        if(inputvector[inputvector[positioncount+1]] < inputvector[positioncount+2]){
          inputvector[inputvector[positioncount+3]] = 1;
          positioncount += 4;
         }
        else{
          inputvector[inputvector[positioncount+3]] = 0;
          positioncount += 4;
        }
      }
      else{
        if(inputvector[inputvector[positioncount+1]] < inputvector[inputvector[positioncount+2]]){
          inputvector[inputvector[positioncount+3]] = 1;
          positioncount += 4;
        }
        else{
          inputvector[inputvector[positioncount+3]] = 0;
          positioncount += 4;
        }
      }
    }
    else if(digits[digits.size()-1] == 8){
      if(parametermodes[0] == 1 && parametermodes[1] == 1){
        if(inputvector[positioncount+1] == inputvector[positioncount+2]){
          inputvector[inputvector[positioncount+3]] = 1;
          positioncount += 4;
        }
        else{
          inputvector[inputvector[positioncount+3]] = 0;
          positioncount += 4;
        }
      }
      else if(parametermodes[0] == 1){
        if(inputvector[positioncount+1] == inputvector[inputvector[positioncount+2]]){
          inputvector[inputvector[positioncount+3]] = 1;
          positioncount += 4;
        }
        else{
          inputvector[inputvector[positioncount+3]] = 0;
          positioncount += 4;
        }
      }
      else if(parametermodes[1] == 1){
        if(inputvector[inputvector[positioncount+1]] == inputvector[positioncount+2]){
          inputvector[inputvector[positioncount+3]] = 1;
          positioncount +=4;
        }
        else{
          inputvector[inputvector[positioncount+3]] = 0;
          positioncount += 4;
        }
      }
      else{
        if(inputvector[inputvector[positioncount+1]] == inputvector[inputvector[positioncount+2]]){
          inputvector[inputvector[positioncount+3]] = 1;
          positioncount += 4;
        }
        else{
          inputvector[inputvector[positioncount+3]] = 0;
          positioncount += 4;
        }
      }
    }

    else if(digits[digits.size()-2] == 9 && digits[digits.size()-1] == 9){
      break;
    }
    count = 0;
    temp = 0;
    digits.erase(digits.begin(), digits.end());
    parametermodes.erase(parametermodes.begin(), parametermodes.end());
  }
}

int main(){
  int opinput = 5;
  string input;
  ifstream inFile ("AdventDay5Input");
  vector<int> inputvector;
  inputvectorlist(inFile, input);
  parseinput(input, inputvector);
  dostuff(inputvector, opinput);
  return 0;
}
