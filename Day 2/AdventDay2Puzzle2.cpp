#include <fstream>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

void inputvectorlist(ifstream& inFile, string& input);
void parseinput(string input, vector<int>& inputvector);
void dostuff(vector<int>& inputvector);

void inputvectorlist(ifstream& inFile, string& input){
  string line;
  if (inFile.is_open()){
    while (getline(inFile, line)){
      input = line;
    }
    inFile.close();
  }
  else cout << "Unable to open file";
}

void parseinput(string input, vector<int>& inputvector){
  stringstream ss(input);
  int count = 0;
  while(ss.good()){
    string substr;
    getline(ss, substr, ',');
    inputvector.resize(count+1);
    inputvector[count] = stoi(substr);
    ++count;
  }
}

void dostuff(vector<int>& inputvector){
  int positioncount = 0;
  int temp;
  while(true){
    if(inputvector[positioncount] == 1){
      temp = inputvector[inputvector[positioncount+1]] + inputvector[inputvector[positioncount+2]];
      inputvector[inputvector[positioncount+3]] = temp;
    }
    else if(inputvector[positioncount] == 2){
      temp = inputvector[inputvector[positioncount+1]] * inputvector[inputvector[positioncount+2]];
      inputvector[inputvector[positioncount+3]] = temp;
    }
    else if(inputvector[positioncount] == 99){
      break;
    }
    positioncount += 4;
  }
}

int main(){
  int noun = 0;
  int verb = 0;
  string input;
  ifstream inFile ("AdventDay2Input");
  vector<int> inputvector;
  vector<int> tempvector;
  inputvectorlist(inFile, input);
  parseinput(input, inputvector);
  for(int noun = 0; noun < 100; ++noun){
    for(int verb = 0; verb < 100; ++verb){
      for(int i = 0; i < inputvector.size(); ++i){
        tempvector.resize(i+1);
        tempvector[i] = inputvector[i];
      }
      tempvector[1] = noun;
      tempvector[2] = verb;
      dostuff(tempvector);
      if(tempvector[0] == 19690720){
        cout << (noun*100)+verb << "\n";
      }
    }
  }
  dostuff(inputvector);
  return 0;
}
