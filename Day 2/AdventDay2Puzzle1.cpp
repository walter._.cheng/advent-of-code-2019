#include <fstream>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

void inputvectorlist(ifstream& inFile, string& input);
void parseinput(string input, vector<int>& inputvector);
void dostuff(vector<int>& inputvector);

void inputvectorlist(ifstream& inFile, string& input){
  string line;
  if (inFile.is_open()){
    while (getline(inFile, line)){
      input = line;
    }
    inFile.close();
  }
  else cout << "Unable to open file";
}

void parseinput(string input, vector<int>& inputvector){
  stringstream ss(input);
  int count = 0;
  while(ss.good()){
    string substr;
    getline(ss, substr, ',');
    inputvector.resize(count+1);
    inputvector[count] = stoi(substr);
    ++count;
  }
}

void dostuff(vector<int>& inputvector){
  int positioncount = 0;
  int temp;
  while(true){
    if(inputvector[positioncount] == 1){
      temp = inputvector[inputvector[positioncount+1]] + inputvector[inputvector[positioncount+2]];
      inputvector[inputvector[positioncount+3]] = temp;
    }
    else if(inputvector[positioncount] == 2){
      temp = inputvector[inputvector[positioncount+1]] * inputvector[inputvector[positioncount+2]];
      inputvector[inputvector[positioncount+3]] = temp;
    }
    else if(inputvector[positioncount] == 99){
      break;
    }
    positioncount += 4;
  }
}

//remember to set position 1 of the input to 12 and position 2 to 2
int main(){
  string input;
  ifstream inFile ("AdventDay2Input");
  vector<int> inputvector;
  inputvectorlist(inFile, input);
  parseinput(input, inputvector);
  dostuff(inputvector);
  cout << inputvector[0];
  return 0;
}
